#!/bin/sh
#
# Tests sur gitlab pipelines.
#
set -e

URL="https://www.bonjourquebec.com/fr-ca/quoi-faire/festivals-et-evenements/fetes-et-festivals"
echo "Tests sur "$URL
OUTPUT=$(curl "$URL")
for string in \
  "Fêtes et festivals" \
  "Voir nos fêtes et festivals" \
; do
  echo ""
  echo ""
  echo "*******"
  echo "Nous cherchons $string dans le code source"
  echo $OUTPUT | grep "$string" >> /dev/null && echo "Nous avons trouvé." || { echo "Nous n'avons pas trouvé; nous quittons avec le code 1"; exit 1; }
  echo "*******"
  echo ""
  echo ""
done
